﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour
{
    [Header("Field of View")]
    [SerializeField] private GameObject _fieldOfView;
    [SerializeField] private LayerMask _playerMask;

    [SerializeField] private Vector3 _openPosition;

    Vector3 _startPosition;
    bool _gateIsOpen = false;
    private float _timer = 2f;
    private void Awake()
    {
        _startPosition = transform.position;
    }
    private void Update()
    {
        LookAtEntry();
        if (_gateIsOpen == true)
            CloseGate();
    }
    private void LookAtEntry()
    {
        Vector3 distanceToPoint = _fieldOfView.transform.position - transform.position;
        Vector3 directionToPoint = distanceToPoint.normalized;

        if (Physics.Raycast(transform.position, directionToPoint, out RaycastHit hit, distanceToPoint.magnitude, _playerMask))
            OpenGate();
    }
    
    private void OpenGate()
    {
        this.transform.position = _openPosition;
        _gateIsOpen = true;
    }
    private void CloseGate()
    {
        _timer -= Time.deltaTime;

        if (_timer <= 0f)
        {
            this.transform.position = _startPosition;
            _timer = 2f;
            _gateIsOpen = false;
        }
    }
}
