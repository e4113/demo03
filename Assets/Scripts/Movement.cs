﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] private LayerMask _obstacleMask;
    [SerializeField] private float _step;

    private PlayerInputActions _playerInputActions;
    private void Awake()
    {
        _playerInputActions = new PlayerInputActions();
        _playerInputActions.Enable();
    }
    private void Update()
    {
        if (_playerInputActions.Game.Up.triggered)
            TryMove(Vector3.forward);
        
        if(_playerInputActions.Game.Down.triggered)
            TryMove(Vector3.back);
        
        if(_playerInputActions.Game.Right.triggered)
            TryMove(Vector3.right);
        
        if(_playerInputActions.Game.Left.triggered)
            TryMove(Vector3.left);
    }
    
    private void TryMove(Vector3 direction)
    {
        var forwardRay = new Ray(transform.position, direction);

        if (Physics.Raycast(forwardRay, out RaycastHit hit, _step, _obstacleMask))
            return;

        transform.forward = direction;
        transform.Translate(direction * _step, Space.World);
    }
}
